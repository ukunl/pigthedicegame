from random import randint

playercount = input("Enter the number of people")
maxscore = 100
safescore = [0] * int(playercount)
player = 0
score=0

while max(safescore) < maxscore:
    print(safescore)
    print("Player %i: Rolling? (Y) " % player)
    rolling = input()

    if rolling == 'y':
        rolled = randint(1, 6)
        print('  Rolled %i' % rolled)
        if rolled == 1:
            print('  Bust! you lose %i but still keep your previous %i'
                  % (score, safescore[player]))
            score, player = 0, (player + 1) % playercount
        else:
            score += rolled
    else:
        safescore[player] += score
        if safescore[player] >= maxscore:
            break
        print('  Sticking with %i' % safescore[player])
        score, player = 0, (player + 1) % playercount

print('\nPlayer %i wins with a score of %i' %(player, safescore[player]))
